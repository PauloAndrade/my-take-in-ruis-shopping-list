class StoredItems {
    #itemContent;
    #store;
    constructor() {
        this.#store = "UnconFusingStoredItemsStore";
        if (!this.#itemContent) {
            const inLocalStore = localStorage.getItem(this.#store);
            this.#itemContent = inLocalStore
                ? JSON.parse(inLocalStore)
                : {};
        }
    }

    set(item) {
        this.#itemContent[item.uuid] = item;
        localStorage.setItem(
            this.#store,
            JSON.stringify(this.#itemContent),
        );
    }
    get() {
        return Object.values(this.#itemContent);
    }
}

export default new StoredItems();