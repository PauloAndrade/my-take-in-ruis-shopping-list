import StoredItems from './StoredItems.js'
import Item from './Item.js'

export default class ItemList {
    #listElement;
    constructor(id = 'shopping_list_wrapper') {
        this.#listElement = this.#createDomElement();
        this.#mountDomElement(id);
        this.#loadStoredToDomElement();
    }

    #createDomElement() {
        const listElement = document.createElement('div');
        listElement.classList.add('daddy-shopping-element');
        return listElement
    }
    #mountDomElement(id) {
        document
            .getElementById(id)
            .appendChild(this.#listElement)
    }
    #loadStoredToDomElement() {
        StoredItems.get()
            .filter(i => !i.deleted)
            .forEach(this.addItem.bind(this))
    }

    addItem(item) {
        const lovelyNewItem = new Item(item);
        const lovelyNewItemElement = lovelyNewItem.element;
        this.#listElement.appendChild(lovelyNewItemElement);
    }
}