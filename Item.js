import StoredItems from './StoredItems.js'

export default class Item {
    #itemContent;
    #itemElement;
    #itemElementValueElement;
    constructor(initialValue) {
        this.#itemContent = 
        typeof initialValue === 'object' 
        ? initialValue 
        : {
            value: initialValue,
            uuid: window.crypto.randomUUID(),
            deleted: null,
            history: [{
                ts: (new Date()).toJSON(),
                value: initialValue
            }]
        };
        this.#createDomElement();
        this.#updateOnStorage();
    }

    get element() {
        return this.#itemElement;
    }

    get content() {
        return this.#itemContent.value;
    }

    #updateOnStorage() {
        StoredItems.set(this.#itemContent);
    }

    #createDomElement() {
        this.#itemElement = document.createElement('li');
        this.#itemElementValueElement = document.createElement('span');
        this.#itemElementValueElement.innerText = this.#itemContent.value;
        this.#itemElement.appendChild(this.#itemElementValueElement);
        const sueSide = document.createElement('span');
        sueSide.innerText = 'X';
        sueSide.addEventListener('click',(e)=>  {
            this.attemptDestroy()
        })
        this.#itemElement.appendChild(sueSide);
    }

    set(content) {
        this.#itemContent.history.push({
            ts: (new Date()).toJSON(),
            value: content
        })
        this.#itemContent.value = content;
        this.#itemElementValueElement.innerText = this.#itemContent;
        this.#updateOnStorage();
    }

    setVisible() {
        this.#itemElement.classList.remove("hidden");
    }

    setInvisible() {
        this.#itemElement.classList.add("hidden");
    }

    attemptDestroy() {
        if (confirm('Remove this Item?')) {
            this.destroy()
        }
    }

    destroy() {
        this.#itemContent.history.push({
            ts: (new Date()).toJSON(),
            value: null
        })
        this.#itemElement.remove()
        this.#itemContent.deleted = (new Date()).toJSON()
        this.#updateOnStorage();
    }
}